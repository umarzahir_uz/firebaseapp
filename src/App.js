import React, { Component } from 'react';
import { BrowserRouter,Route,Switch } from 'react-router-dom';
import Navbar from './layout/navbar';
import Dashboard from './dashboard/dashboard';
import NewData from './dashboard/newData';
import Detail from './dashboard/detail';
import siginForm from './dashboard/siginForm';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
      <div className="App">
       <Navbar/>
       <Switch>
       <Route exact path="/" component={Dashboard}></Route>
       <Route  path="/newData" component={NewData}></Route>
       <Route  path="/user/:id" component={Detail}></Route>
       <Route  path="/signin" component={siginForm}></Route>
       
       </Switch>
      </div>
      </BrowserRouter>
    );
  }
}

export default App;
