import React, { Component } from 'react'
import {Link} from 'react-router-dom'

export default class signoutLinks extends Component {
  render() {
    return (
      <div>
        <ul className="navbar-nav ml-auto">
            <li className="nav-item"><Link className="nav-link" to="/signin">SignIn</Link></li>
            <li className="nav-item"><Link className="nav-link" to="/signUp">SignUp</Link></li>
        </ul>
      </div>
    )
  }
}
