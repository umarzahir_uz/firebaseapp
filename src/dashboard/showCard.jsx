import React, { Component } from 'react'
import Card from './card';

export default class showCard extends Component {
  render() {
    return (
      <div className="container-fluid">
      <div className="row d-flex justify-content-center">
      
      {this.props.data && this.props.data.map(ind => {
          return (
            <div className=" col-sm-8"  key={ind.id} >
            <Card cardData={ind}/>
            </div>
          )
        })}
      </div>
      </div>
    )
  }
}

