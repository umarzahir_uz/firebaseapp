import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import {logoutAction} from './../store/actions/authAction'

class SigninLinks extends Component {
  render() {
    return (
      <div>
        <ul className="navbar-nav ml-auto">
            <li className="nav-item">< Link className="nav-link" to="newData">Add_New</Link></li>
            <li className="nav-item"><Link className="nav-link" to="/signin" onClick={this.props.logout}>SignOut</Link></li>
            <li className="nav-item"><Link className="nav-link" to="/">NN</Link></li>
        </ul>
      </div>
    )
  }
}
const mapDispatchToProps = (dispatch) =>
{
    return {
        logout:() => dispatch(logoutAction())
    }
}

export default connect(null,mapDispatchToProps)(SigninLinks)