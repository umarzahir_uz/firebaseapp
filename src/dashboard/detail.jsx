import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {compose} from 'redux'
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';


class detail extends Component {
  render() {
      console.log("detail page state",this.props)
     const dd = this.props.info
     if(dd){
    return (
      <div>
        <div className="card m-4">
        <div className="card-body bg-info">
        <p className="card-title ">{dd.name}</p>
        <p className="card-text">{dd.info}</p>
        <p className="card-action">{dd.detail}</p>
        <Link to="/">Go Back</Link>
        </div>
        </div>
      </div>
    )}else
    return(
        <div className="container">
            <h1>Loding Detail.....</h1>
        </div>
    )
  }
}


const mapStateToProps = (state, ownProps) =>
{
    console.log("detail",state)
  const id = ownProps.match.params.id
  console.log("detail page id",id)
  const informations = state.firestore.data.information 
  const info = informations ? informations[id] : null
  return {
    info: info
  }
}
export default  compose(
    connect(mapStateToProps),
  firestoreConnect([{
    collection: 'information'
  }])
  )(detail)