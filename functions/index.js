const functions = require('firebase-functions');
const admin = require('firebase-admin');
    admin.initializeApp(functions.config().firebase)

exports.helloWorld = functions.https.onRequest((request, response) => {
 response.send("Hello from Firebase!");
});


const createNotification = (notification =>{
    return admin.firestore().collection('notifications')
    .add(notification)
    .then(doc => {console.log('Notification added',doc)})
})

exports.projectCreated = functions.firestore.document('information/{projectId}').onCreate(doc =>
    {
        const project = doc.data();
        const notification = {
            content: 'New Info is added.',
            user: `${project.name}`,
            time: admin.firestore.FieldValue.serverTimestamp()
        }

        return createNotification(notification)
    })
