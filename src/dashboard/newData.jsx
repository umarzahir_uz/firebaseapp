import React, { Component } from 'react'
import {connect} from 'react-redux'
import { dataAction } from './../store/actions/dataAction';

 class newData extends Component {
    state={
        name: "",
        info: "",
        detail: "",
        
    }
    handleChange = (event) => 
    {
        this.setState({
            [event.target.id]: event.target.value
        })
    }
    handleSubmit = (event) =>
    {
        event.preventDefault()
        this.props.addData(this.state)
        this.props.history.push('/')
    }
  render() {
    
    return (
      <div className="container bg-warning darken-4 mt-4 rounded">
       <form onSubmit={this.handleSubmit} >
  <div className="form-group pt-4">
    <label htmlFor="exampleFormControlInput1">Name</label>
    <input onChange={this.handleChange} type="text" className="form-control border border-primary" id="name" placeholder="Name"/>
  </div>
  <div className="form-group">
    <label htmlFor="exampleFormControlInput1">Info</label>
    <input onChange={this.handleChange} type="text" className="form-control border border-success" id="info" placeholder="Info"/>
  </div>
  <div className="form-group">
    <label htmlFor="exampleFormControlTextarea1">Detail</label>
    <textarea onChange={this.handleChange} className="form-control border border-dark" id="detail" rows="3" placeholder="Detail"></textarea>
  </div>
  <div>
      <button className="btn btn-success mb-2 ">Create</button>
  </div>
</form>
      </div>
    )
  }
}
const mapDispatchToProps = (dispatch)=>
{
 return{
   addData: (newdata) => dispatch(dataAction(newdata))
 }
}
export default connect(null,mapDispatchToProps)(newData)