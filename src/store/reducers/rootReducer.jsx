import {combineReducers} from 'redux'
import dataReducer from './dataReducer';
import {firestoreReducer} from 'redux-firestore'
import {firebaseReducer} from 'react-redux-firebase'
import authReducer from './authReducer';

const rootReducer = combineReducers({
    dataReducer: dataReducer,
    firestore: firestoreReducer,
    firebase: firebaseReducer,
    auth: authReducer
})
    
export default rootReducer