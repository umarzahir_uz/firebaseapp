import React, { Component } from 'react'
import ShowCard from './showCard';
import {firestoreConnect} from 'react-redux-firebase'
import {compose} from 'redux'
import { connect } from 'react-redux';
import Modal from './modal';
import {Redirect} from 'react-router-dom'
import Notification from './notification';

 class dashboard extends Component {
  render() {
  if(!this.props.auth.uid){ return <Redirect to="/signin"/>}
  if(this.props.userData)
  {
    return (
      <div className="container">
      <div className="row">
      <div className="col-sm-8">
      <ShowCard data={this.props.userData}/>
    { this.props.show ? <Modal data={this.props.data}/> : null }
      </div>
      <div className="col-sm-4">
      <Notification notification={this.props.notification}/>
      </div>
      
      </div>
      </div>
    )
  }else{
    return(
      <div className="container d-flex justify-content-center align-content-center">
          <div className="spinner-grow text-primary" role="status">
  <span className="sr-only">Loading...</span>
</div>
<div className="spinner-grow text-secondary" role="status">
  <span className="sr-only">Loading...</span>
</div>
<div className="spinner-grow text-success" role="status">
  <span className="sr-only">Loading...</span>
</div>
<div className="spinner-grow text-danger" role="status">
  <span className="sr-only">Loading...</span>
</div>
<div className="spinner-grow text-warning" role="status">
  <span className="sr-only">Loading...</span>
</div>
<div className="spinner-grow text-info" role="status">
  <span className="sr-only">Loading...</span>
</div>
<div className="spinner-grow text-light" role="status">
  <span className="sr-only">Loading...</span>
</div>
<div className="spinner-grow text-dark" role="status">
  <span className="sr-only">Loading...</span>
</div>
      </div>
    )
  }
  }
}

const mapStateToProps = (state)=>
{

  return {
    userData: state.firestore.ordered.information,
    show: state.dataReducer.show,
    data: state.dataReducer.mdata,
    auth: state.firebase.auth,
    notification: state.firestore.ordered.notifications
  }
}

export default compose(connect(mapStateToProps),
firestoreConnect([
  {collection: 'information', orderBy: ['createdAt' , 'desc']},
  {collection: 'notifications' , limit: 5, orderBy: ['time','desc']}
])
)(dashboard)