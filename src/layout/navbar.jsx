import React from 'react'
import {Link} from 'react-router-dom'
import SigninLinks from './SigninLinks';
import SignoutLinks from './../dashboard/signoutLinks';
import { connect } from 'react-redux'
import  { Component } from 'react'


 class navbar extends Component {
  render() {
    return (
      <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <Link to="/" className="navbar-brand">Uk-DB</Link>
      {this.props.auths.uid ?  <SigninLinks/> :  <SignoutLinks/>   }
    
       
      
      </nav>
    </div>
    )
  }
}

const mapStateToProps = (state)=>
{
  console.log("navbar page state",state)
  return{
    auths: state.firebase.auth
  }
}



export default connect(mapStateToProps)(navbar)
