export const delAction = (delId)=>
{
  return (dispatch,getstate , {getFirebase, getFirestore}) =>
  {
     const firestore = getFirestore();
     firestore.collection("information").doc(delId).delete()
     .then(dispatch({type: "DEL_PASS"}))
     .catch((err)=>{dispatch({type: "DEL_FAIL"},err)})
  }
}