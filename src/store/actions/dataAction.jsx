export const dataAction = (project) =>
{
    return (dispatch, getState, {getFirebase, getFirestore}) =>
    {
        const firestore = getFirestore()
        firestore.collection('information').add({
            ...project,
            createdAt: new Date()
        }).then(()=>
        dispatch({ type: 'DATA_PASS', project })
        ).catch((err)=>{
            dispatch({ type: 'DATA_FAIL', err })
        })
    }
}