/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { delAction } from './../store/actions/delAction';

 class card extends Component {
    state = {
        src: "No Image",
    }
   
  render() {
    const cdata = this.props.cardData
   const handleShow = ()=>
    {
      this.props.modalData(cdata)
      this.props.changeShow()
      
    }
    const handleDelete=()=>
    {
       this.props.del(cdata.id)
    }

    
    console.log("card page",this.state.md)
    return (
      <div>
        <div className="card sm-8 mt-4">
        <img  className="card-img-top bg-success" src={this.state.src} alt="Card image "></img>
        <div className="card-body bg-warning">
        <p className="card-title ">{cdata.name}</p>
        <p className="card-text">{cdata.info}</p>

        <div className="card-action d-flex justify-content-around">
        <Link to={"/user/" + cdata.id}><button type="button" className="btn btn-primary btn-sm">Detail</button></Link>
        <button className="btn  btn-sm btn-success" onClick={handleShow} >Edit</button>
        <button className="btn  btn-sm btn-danger" onClick={handleDelete} >Del</button>
        </div>
        </div>
        </div>
      </div>
    )
  }
}
const mapDispatchToProps = (dispatch)=>{
  return{
    changeShow: () => {dispatch({type: "UPDATE_SHOW"})},
    modalData: (cdata) => {dispatch({type: "MODAL_DATA", dm: cdata})},
    del: (delId) => {dispatch(delAction(delId))}
  }
  }

  export default  connect(null,mapDispatchToProps)(card)